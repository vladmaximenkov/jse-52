package ru.vmaksimenkov.tm.listener;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.service.IActiveMQConnectionService;
import ru.vmaksimenkov.tm.enumerated.EventType;
import ru.vmaksimenkov.tm.service.ActiveMQConnectionService;

import javax.persistence.*;

import static ru.vmaksimenkov.tm.enumerated.EventType.*;

public final class EntityListener {

    @NotNull
    private final IActiveMQConnectionService activeMQConnectionService = new ActiveMQConnectionService();

    @PostLoad
    public void postLoad(@NotNull final Object record) {
        sendMessage(record, LOAD);
    }

    @PrePersist
    public void prePersist(@NotNull final Object record) {
        sendMessage(record, START_PERSIST);
    }

    @PostPersist
    public void postPersist(@NotNull final Object record) {
        sendMessage(record, FINISH_PERSIST);
    }

    @PreUpdate
    public void preUpdate(@NotNull final Object record) {
        sendMessage(record, START_UPDATE);
    }

    @PostUpdate
    public void postUpdate(@NotNull final Object record) {
        sendMessage(record, FINISH_UPDATE);
    }

    @PreRemove
    public void preRemove(@NotNull final Object record) {
        sendMessage(record, START_REMOVE);
    }

    @PostRemove
    public void postRemove(@NotNull final Object record) {
        sendMessage(record, FINISH_REMOVE);
    }

    public void sendMessage(@NotNull final Object entity, @NotNull final EventType event) {
        activeMQConnectionService.send(entity, event);
    }

}
