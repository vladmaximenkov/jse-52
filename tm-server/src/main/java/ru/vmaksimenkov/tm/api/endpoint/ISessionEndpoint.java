package ru.vmaksimenkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.dto.UserRecord;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @WebMethod
    boolean closeSession(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session
    );

    @Nullable
    @WebMethod
    UserRecord getUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session
    );

    @Nullable
    @WebMethod
    SessionRecord openSession(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password
    );

}
