package ru.vmaksimenkov.tm.exception.user;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
