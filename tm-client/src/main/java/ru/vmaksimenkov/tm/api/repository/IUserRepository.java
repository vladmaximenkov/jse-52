package ru.vmaksimenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.endpoint.UserRecord;

public interface IUserRepository extends IRepository<UserRecord> {

    boolean existsByEmail(@Nullable String email);

    boolean existsById(@NotNull String id);

    boolean existsByLogin(@Nullable String login);

    @Nullable
    UserRecord findById(@NotNull String id);

    @Nullable
    UserRecord findByLogin(@NotNull String login);

    void removeByLogin(@NotNull String login);

    void setPasswordById(@NotNull String id, @NotNull String password);

}
