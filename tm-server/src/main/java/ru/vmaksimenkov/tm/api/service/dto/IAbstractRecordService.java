package ru.vmaksimenkov.tm.api.service.dto;

import ru.vmaksimenkov.tm.api.repository.dto.IAbstractRecordRepository;
import ru.vmaksimenkov.tm.dto.AbstractEntityRecord;

public interface IAbstractRecordService<E extends AbstractEntityRecord> extends IAbstractRecordRepository<E> {

}
