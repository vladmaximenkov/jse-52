package ru.vmaksimenkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    void clearProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session
    );

    @WebMethod
    Long countProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session
    );

    @WebMethod
    ProjectRecord createProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @Nullable String description
    );

    @WebMethod
    boolean existsProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    boolean existsProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @Nullable
    @WebMethod
    List<ProjectRecord> findProjectAll(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session
    );

    @Nullable
    @WebMethod
    ProjectRecord findProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @Nullable
    @WebMethod
    ProjectRecord findProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @Nullable
    @WebMethod
    ProjectRecord findProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void finishProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    void finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @WebMethod
    void finishProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void removeProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    void removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @WebMethod
    void removeProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void setProjectStatusById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @WebMethod
    void setProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @WebMethod
    void setProjectStatusByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @WebMethod
    void startProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    void startProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @WebMethod
    void startProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void updateProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

    @WebMethod
    void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

    @WebMethod
    void updateProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "nameNew", partName = "nameNew") @NotNull String nameNew,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

}
