package ru.vmaksimenkov.tm.api.service.model;

import ru.vmaksimenkov.tm.api.repository.model.IAbstractBusinessRepository;
import ru.vmaksimenkov.tm.model.AbstractBusinessEntity;

public interface IAbstractBusinessService<E extends AbstractBusinessEntity> extends IAbstractBusinessRepository<E> {

}
