package ru.vmaksimenkov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.ProjectRecord;

public interface IProjectRecordRepository extends IAbstractBusinessRecordRepository<ProjectRecord> {

    boolean existsByName(@Nullable String userId, @Nullable String name);

    @Nullable
    ProjectRecord findByName(@Nullable String userId, @Nullable String name);

    @Nullable
    String getIdByName(@Nullable String userId, @Nullable String name);

    void removeByName(@Nullable String userId, @Nullable String name);

}
